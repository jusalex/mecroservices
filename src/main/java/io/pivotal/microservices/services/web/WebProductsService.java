package io.pivotal.microservices.services.web;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Service
public class WebProductsService {

    @Autowired
    @LoadBalanced
    protected RestTemplate restTemplate;

    protected String serviceUrl;

    protected Logger logger = Logger.getLogger(WebProductsService.class
            .getName());

    public WebProductsService(String serviceUrl) {
        this.serviceUrl = serviceUrl.startsWith("http") ? serviceUrl
                : "http://" + serviceUrl;
    }

    /**
     * The RestTemplate works because it uses a custom request-factory that uses
     * Ribbon to look-up the service to use. This method simply exists to show
     * this.
     */
    @PostConstruct
    public void demoOnly() {
        // Can't do this in the constructor because the RestTemplate injection
        // happens afterwards.
        logger.warning("The RestTemplate request factory is "
                + restTemplate.getRequestFactory().getClass());
    }

    public List<Product> findByName(String productName) {
        Product[] prods = restTemplate.getForObject(serviceUrl + "/products/find/{productName}",
                Product[].class, productName);

        if (prods == null || prods.length == 0) {
            return null;
        } else {
            return Arrays.asList(prods);
        }
    }

    public List<Product> findAll() {
        Product[] prods = restTemplate.getForObject(serviceUrl + "/products",
                Product[].class);

        if (prods == null || prods.length == 0) {
            return null;
        } else {
            return Arrays.asList(prods);
        }
    }

    public Product findById(Long productId) {
        return restTemplate.getForObject(serviceUrl + "/products/{productId}",
                Product.class, productId);
    }

    public Product create(Product product) {
        return restTemplate.postForObject(serviceUrl + "/products", product,
                Product.class);
    }

    public void update(Product product) {
        restTemplate.put(serviceUrl + "/products", product);
    }

    public void delete(Long productId) {
        restTemplate.delete(serviceUrl + "/products/{productId}", productId);
    }
}
