package io.pivotal.microservices.services.web;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class WebProductsController {

    @Autowired
    protected WebProductsService webProductsService;

    protected Logger logger = Logger.getLogger(WebProductsController.class
            .getName());

    public WebProductsController(WebProductsService webProductsService) {
        this.webProductsService = webProductsService;
    }

    @RequestMapping("/products")
    public String goHome(Model model) {
        List<Product> products = webProductsService.findAll();
        if (products != null)
            model.addAttribute("products", products);
        return "products";
    }

    @RequestMapping("/products/{productId}")
    public String byId(Model model,
                       @PathVariable("productId") Long productId) {

        Product product = webProductsService.findById(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @RequestMapping("/products/find/{text}")
    public String nameSearch(Model model, @PathVariable("text") String name) {
        List<Product> products = webProductsService.findByName(name);
        model.addAttribute("search", name);
        if (products != null)
            model.addAttribute("products", products);
        return "products";
    }

    @RequestMapping(value = "/products/search", method = RequestMethod.GET)
    public String searchForm(Model model) {
        model.addAttribute("productSearchCriteria", new ProductsSearchCriteria());
        return "productSearch";
    }

    @RequestMapping(value = "/products/dosearch")
    public String doSearch(Model model, ProductsSearchCriteria criteria,
                           BindingResult result) {
        criteria.validate(result);

        if (result.hasErrors()) {
            model.addAttribute("productSearchCriteria", criteria);
            return "productSearch";
        }

        String productId = criteria.getProductNumber();
        if (StringUtils.hasText(productId)) {
            return byId(model, Long.parseLong(productId));
        } else {
            String searchText = criteria.getSearchText();
            return nameSearch(model, searchText);
        }
    }

    @RequestMapping("/products/remove/{id}")
    public String removeProduct(@PathVariable("id") Long id) {
        webProductsService.delete(id);
        return "redirect:/products";
    }

    @RequestMapping("/products/edit/{id}")
    public String editProduct(@PathVariable("id") Long id, Model model){
        model.addAttribute("product", webProductsService.findById(id));
        return "editProduct";
    }

    @RequestMapping(value = "/products/saveProduct", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute("product") Product product){
        webProductsService.update(product);
        return "redirect:/products";
    }
}
