package io.pivotal.microservices.products;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import io.pivotal.microservices.accounts.Account;
import io.pivotal.microservices.accounts.AccountRepository;
import io.pivotal.microservices.exceptions.AccountNotFoundException;
import io.pivotal.microservices.exceptions.ProductNotFoundException;

@RestController
public class ProductsController {

	protected Logger logger = Logger.getLogger(ProductsController.class
			.getName());
	protected ProductRepository productRepository;

	@Autowired
	public ProductsController(ProductRepository productRepository) {
		this.productRepository = productRepository;

		logger.info("ProductRepository says system has "
				+ productRepository.countProducts() + " products");
	}

	@RequestMapping("/products/{productId}")
	public Product findById(@PathVariable("productId") Long productId) {
		Product product = productRepository.findOne(productId);
		if (product == null)
			throw new ProductNotFoundException(productId);
		else {
			return product;
		}
	}

	@RequestMapping("/products")
	public List<Product> getAll() {
        Iterable<Product> all = productRepository.findAll();
        List<Product> target = new ArrayList<>();
        all.forEach(target::add);
        return target;
	}

	@RequestMapping("/products/find/{name}")
	public List<Product> findByName(@PathVariable("name") String partialName) {
		List<Product> products = productRepository
				.findByNameContainingIgnoreCase(partialName);

		if (products == null || products.size() == 0)
			throw new ProductNotFoundException(partialName);
		else {
			return products;
		}
	}

	@RequestMapping(value = "/products/", method = RequestMethod.POST)
	public Product create(@RequestBody Product product) {
		return productRepository.save(product);
	}

	@RequestMapping(value = "/products/{productId}", method = RequestMethod.DELETE)
	public void delete(@PathVariable("productId") Long productId) {
		productRepository.delete(productId);
	}

	@RequestMapping(value = "/products", method = RequestMethod.PUT)
	public Product update(@RequestBody Product product) {
		return productRepository.save(product);
	}
}
